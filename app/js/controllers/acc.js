(function () {
    'use strict';
    angular.module('App').controller('AccController', AccController);
    AccController.$inject = ['$scope', '$stateParams', '$timeout'];

    function AccController($scope, $stateParams, $timeout) {
        var current = 0;
        var max = 5;
        $scope.consola = [];
        $scope.consola.push('run');
        $scope.acc = {
            x: 0,
            y: 0,
            z: 0,
            t: 0
        };
        $scope.color = $stateParams.color;
        if (navigator.accelerometer && navigator.accelerometer.getCurrentAcceleration) {
            $scope.consola.push('navigator.accelerometer');
            getData();
        }
        function getData() {
            navigator.accelerometer.getCurrentAcceleration(onSuccess, onError);

            if (current++ < max) {
                $timeout(getData, 2000);
            } else {
                $scope.consola.push('the end' + current + ' ' + max);
            }
        }

        function onSuccess(acceleration) {
            $scope.acc = {
                x: acceleration.x,
                y: acceleration.y,
                z: acceleration.z,
                t: acceleration.timestamp
            };
        }

        function onError() {
            $scope.consola.push('error');
        }
    }
})();